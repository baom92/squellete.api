##### Importation obligatoire. A ne pas modifier #####
import logging
import uuid
import json
from datetime import datetime
from flask import request
from flask_restplus import Resource
from controllers.restplus import api
from repositories.models import mongoConnection
##### Fin Importation obligatoire. A ne pas modifier #####

##### Importation des serializers #####
from controllers.serializers import post, get
##### Importation des serializers #####

##### Chargement des paramètres de l'API #####
with open('../ressources/parametres.json', 'r') as fich_p:
    parameters = json.loads(fich_p.read())

MONGO_IP = parameters['ipDatabase']
MONGO_DATABASE = parameters['nameDatabase']
##### Fin Chargement des paramètres de l'API #####

##### Initialisation du controlleur. A ne pas modifier #####
log = logging.getLogger(__name__)
##### Fin Initialisation du controlleur #####

##### Création du namespace. Il représente le controlleur et sera appellé 
##### par la classe Main de notre API. Il faut le modifier pour l'adapter
##### à la ressource manipuler.
##### ns = api.namespace(<nom_ressource>, description=<Description de la ressource>)#####
ns = api.namespace('recipes', description='Operations related to recipes') ## Un exemple de namespace
##### Fin Création du namespace #####


##### Initialisation de la connexion à MongoDb ##### 
collection = mongoConnection(MONGO_IP, MONGO_DATABASE, <nom_ressource>)
##### Fin Initialisation du controlleur #####


##### Création des méthodes liées à la ressource #####
##### Par défaut, une ressource a 5 méthodes  :  #####
##### 1.) Création de la ressource
##### 2.) Modification de la ressource
##### 3.) Lecture de la ressource
##### 4.) Suppression de la ressource
##### 5.) Recherche d'une ressource

@ns.route('/')
class Collection(Resource):

    ##### 5.) Recherche d'une ressource
    @api.marshal_list_with(get)
    def get(self):
        """
            Returns list of recipes
        """
        recettes = list(collection.find({}))
        return recettes

    ##### 1.) Création d'une ressource
    @api.response(201, 'Category successfully created.')
    @api.expect(post)
    def post(self):
        """
        Creates a new recipe
        """
        data = request.json
        data['_id'] = str(uuid.uuid4())
        collection.insert_one(data)
        return None, 201


@ns.route('/<string:id>')
@api.response(404, 'Recipe not found.')
class Item(Resource):

    ##### 3.) Lecture d'une ressource
    @api.marshal_with(get)
    def get(self, id):
        """
            Returns a recipe with a list of posts.
        """
        return collection.find_one({"_id": id})

    ##### 2.) Mise à jour d'une ressource
    @api.response(204, 'Recipe successfully updated.')
    @api.expect(post)
    def put(self, id):
        """
        Updates a recipe
        """
        data = request.json
        data['_id'] = id
        collection.replace_one(collection.find_one({'_id': id}), data, False)
        return None, 204

    ##### 4.) Suppression d'une ressource
    @api.response(204, 'Recipe successfully deleted.')
    def delete(self, id):
        """
        Deletes blog category.
        """
        collection.delete_one(collection.find_one({'_id': id}))
        return None, 204
