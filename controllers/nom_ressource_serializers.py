##### Importation obligatoire. A ne pas modifier #####
from flask_restplus import fields
from controllers.restplus import api
##### Fin Importation obligatoire. A ne pas modifier #####

##### Ici, on doit décrire à nos futures utilisateurs chaque ressource.
##### La description se fait de la manière suivante :
##### int : fields.Integer(required=<True or False>, description=<Description de la variable>)
##### string : fields.String(required=<True or False>, description=<Description de la variable>)
##### float : fields.Float(required=<True or False>, description=<Description de la variable>)
##### objet (JSON embarqué) : fields.Nested(<nom_serializer>) 
##### liste simple (liste de int, string, ...) : fields.List(fields.String, required=<True or False>, description=<Description de la variable>)
##### liste d'objet : fields.List(fields.Nested(<nom_serializer>), required=<True or False>, description=<Description de la variable>)

etape_preparation = api.model('A recipe preparation step', {
    'numero': fields.Integer(required=True, description='Index of the step'),
    'description': fields.String(required=True, description='The step description')
})

post = api.model('Recipe post', {
    "nom": fields.String(required=True, description='The recipe name'),
    "nombre_personnes": fields.String(required=True, description='The number of people'),
    "temps_preparation": fields.String(required=True, description='The preparation time'),
    "temps_cuisson": fields.String(required=True, description='The cooking time'),
    "temps_total": fields.String(required=True, description='The total duration'),
    "ingredients": fields.List(fields.String,required=True, description='The recipe ingredients list'),
    "etapes_preparation": fields.List(fields.Nested(etape_preparation), required=True,
                                      description='The recipe preparation steps')
})

get = api.model('Recipe get', {
    '_id': fields.String(readOnly=True, description='The unique identifier of a recipe'),
    "nom": fields.String(required=True, description='The recipe name'),
    "nombre_personnes": fields.String(required=True, description='The number of people'),
    "temps_preparation": fields.String(required=True, description='The preparation time'),
    "temps_cuisson": fields.String(required=True, description='The cooking time'),
    "temps_total": fields.String(required=True, description='The total duration'),
    "ingredients": fields.List(fields.String,required=True, description='The recipe ingredients list'),
    "etapes_preparation": fields.List(fields.Nested(etape_preparation), required=True,
                                      description='The recipe preparation steps')
})
