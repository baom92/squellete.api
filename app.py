##### Importation obligatoire. A ne pas modifier #####
import logging.config
import os
from flask import Flask, Blueprint
from controllers.restplus import api
##### Fin Importation obligatoire. A ne pas modifier #####

##### Importation des namespaces créer au niveau des controlleurs #####
from controllers.recette import ns as namespace
##### Fin Importation des namespaces créer au niveau des controlleurs #####

##### Chargement des paramètres de l'API #####
with open('../ressources/parametres.json', 'r') as fich_p:
    parameters = json.loads(fich_p.read())

SERVER_PORT = parameters['serverPort']
FLASK_SERVER_NAME = 'localhost:'+str(SERVER_PORT)
##### Fin Chargement des paramètres de l'API #####

# Flask-Restplus settings
# A ne pas modifier
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

app = Flask(__name__)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), 'logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)
# Fin settings

## Configuration de l'API. A ne pas modifier
def configure_app(flask_app):
    flask_app.config['SERVER_NAME'] = FLASK_SERVER_NAME
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = RESTPLUS_ERROR_404_HELP
## Fin Configuration de l'API. A ne pas modiifer

## Initialisation de l'API
def initialize_app(flask_app):
    ## A ne pas modifier
    configure_app(flask_app)
    
	## A ne pas modifier
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    
	## A ne pas modifier
    api.init_app(blueprint)

    ## Ajout des namespaces à l'API
    api.add_namespace(namespace)

    ## A ne pas modifier
    flask_app.register_blueprint(blueprint)
## Fin Initialisation de l'API

## Démmarage de l'API. A ne pas mofifier
def main():
    initialize_app(app)
    log.info('>>>>> Starting development server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run(debug=True)
## Fin Démmarage de l'API. A ne pas mofifier


if __name__ == "__main__":
    main()
