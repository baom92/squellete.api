Une API CRUD (Create, Read, Update, Delete) contient, la pluspart du temps, 5 méthodes par ressource. Une ressource, c'est l'élément ou l'un des éléments que manipule l'API.
Par exemple, une API de gestion de stock va manipuler les ressources "Stock" et "Users". La ressource "Stock" peut contenir les classes "Produit" et "Transaction". Il convient
de faire la différence entre ressource et classe car bien souvent, une ressource contient plusieurs classes.


|   app.py----------------------------------------------Fichier principale de l'API
|   logging.conf----------------------------------------Paramètres des logs
|   requirements.txt------------------------------------Les paquages à installer
|   
+---controllers
|       <nom_ressource>.py------------------------------Le fichier controlleur de la ressource
|       restplus.py-------------------------------------Fichier de configuration de l'API
|       <nom_ressource>_serializers.py------------------Fichier de configuration swagger
|       __init__.py
|       
+---repositories
|       models.py---------------------------------------Fichier de communication avec la DB
|       __init__.py
|       
\---ressources
|       parameters.json---------------------------------Le fichier contenant les paramètres externes à l'API
        __init__.py