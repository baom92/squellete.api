from pymongo import MongoClient
import traceback

def mongoConnection(mongo_url, mongo_database, collection):
    try:
        url = "mongodb://" + mongo_url + ":27017"
        conn = MongoClient(url)

        # database
        db = conn[mongo_database]

        # Created or Switched to the given collection
        return db[collection]
    except Exception:
        print(traceback.format_exc())
        return None